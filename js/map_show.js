(function ($) {
  Drupal.Leaflet.prototype.create_json = function (json) {
    var geojsonMarkerOptions = {
      radius: 12,
      fillColor: "#bbb",
      color: "#000",
      weight: 4,
      opacity: 1,
      fillOpacity: 0.2
    };
    lJSON = new L.GeoJSON();
    lJSON.options.onEachFeature = function(feature, layer){
      for (var layer_id in layer._layers) {
        for (var i in layer._layers[layer_id]._latlngs) {
          Drupal.Leaflet.bounds.push(layer._layers[layer_id]._latlngs[i]);
        }
      }
      if (feature.properties.style) {
        layer.setStyle(feature.properties.style);
      }
      if (feature.properties.leaflet_id) {
        layer._leaflet_id = feature.properties.leaflet_id;
      }
      if (feature.properties.popup) {
        layer.bindPopup(feature.properties.popup);
      }
      if (feature.type == "Point"){
        //layer.
      }
    }
    lJSON.options.pointToLayer = function (feature, latlng) {
      return L.circleMarker(latlng, geojsonMarkerOptions);
    };
    lJSON.addData(json);

		var svgLayer = L.svg();
		lJSON.addLayer(svgLayer);//;.addTo(lJSON._map);

    return lJSON;
  };

  $(document).ready(function () {
    var svg = d3.select("#leaflet-map").select("svg");
		var g = d3.select("#leaflet-map").select("svg").select('g');
		g.attr("class", "leaflet-zoom-hide");
    //TODO GET DATA FROM ALREADY THERE JSON; collection = d3.data(lJSON.toGeoJSON());
	  d3.json('https://mev.sb.communia.org/d3_show/json/cities').then(function(collection) {
			// Add a LatLng object to each item in the dataset
			collection.forEach(function(d) {
							d.LatLng = new L.LatLng(d.geometry.coordinates[0],
																			d.geometry.coordinates[1])
							d.radius = d.properties.entities_q;
			})
			var feature = g.selectAll("circle")
				.data(collection)
				.enter().append("circle")
				.style("stroke", "none")  
				.style("opacity", .4) 
				.style("fill", "red")
				.attr("r", function(d,i) {return 30+d.properties.entities_q*5});
		  var texts = g.selectAll("g")
				.data(collection)
				.attr('x', '50%')
				.attr('y', '50%')
 				.enter().append("text")
	      .attr('text-anchor', 'middle')
        .attr('font-size', '18px')
        .attr('dy', '.em')
				.style("color", "blue")
        .text(function(d) { return d.properties.city; }); 
        //.attr("font", "bold 30px serif")

			
    console.log(feature);

			lJSON._map.on("moveend", update);
			lJSON._map.on("zoomend", updateZoom);
			lJSON._map.setView([41.555, 2.15], 12.38);
		
			update();

			function update() {
				feature.attr("transform", 
				function(d) { 
					return "translate("+ 
						lJSON._map.latLngToLayerPoint(d.LatLng).x +","+ 
						lJSON._map.latLngToLayerPoint(d.LatLng).y +")";
					}
				)
				texts.attr("transform", 
				function(d) { 
console.log( "translate("+ lJSON._map.latLngToLayerPoint(d.LatLng).x +","+ lJSON._map.latLngToLayerPoint(d.LatLng).y +") "+ "scale( " + (100/19 * lJSON._map._zoom) / 100 +")");
					return "translate("+ 
						lJSON._map.latLngToLayerPoint(d.LatLng).x +","+ 
						lJSON._map.latLngToLayerPoint(d.LatLng).y +")"; //+
//						"scale(" + (100/19 * lJSON._map._zoom) / 100 +")";
					}
				)
/*				texts.attr("transform",
				function(d) { 
					return "scale( " + (100/19 *
					lJSON._map._zoom) / 100 +")";
				})
*/
			}
	  })
  });	
 
})(jQuery);
