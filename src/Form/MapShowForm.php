<?php

namespace Drupal\d3_show\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \GuzzleHttp\Client;
use Drupal\leaflet\LeafletService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MapShowForm.
 */
class MapShowForm extends FormBase {
  /**
   * @var LeafletService $leaflet
   */
  protected $leaflet;

 /**
   * Guzzle Http Client.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

 /**
	* The endpoint url to get the json to feed the map
	*
	* @var string
	*/
  protected $entitiesUrl = 'https://mev.sb.communia.org/d3_show/json/topojson';

 /**
   * Constructs a new Class.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http_client.
   */
	public function __construct( LeafletService $leaflet_service, Client $http_client ){
    $this->leaflet = $leaflet_service;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static($container->get('leaflet.service'), $container->get('http_client'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'map_show_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $maps = [];
    $maps['mapnik'] = leaflet_map_get_info()['OSM Mapnik'];
		$default_settings = array(
			'attributionControl' => TRUE,
			'closePopupOnClick'  => TRUE,
			'doubleClickZoom'    => TRUE,
			'dragging'           => TRUE,
			'fadeAnimation'      => TRUE,
			'layerControl'       => FALSE,
			'maxZoom'            => 19,
			'minZoom'            => 0,
			'scrollWheelZoom'    => TRUE,
			'touchZoom'          => TRUE,
			'trackResize'        => TRUE,
			// Don't specify, if you want to use Auto-box.
			'zoom'               =>  14,
			'zoomAnimation'      => TRUE,
      'zoomControl'        => FALSE,
		);
    $stamen_code = ['Watercolor', 'watercolor']; //could be Watercolor or terrain, or Toner
    $maps['stamen'] = [
      'label' => "Stamen $stamen_code[0]", 
      'description' => "Stamen $stamen_code[0] layer.",
      'settings' => $default_settings,
      'layers' => [
        'layer' => [
          'urlTemplate' => "https://stamen-tiles-{s}.a.ssl.fastly.net/$stamen_code[1]/{z}/{x}/{y}.png",
          'options' => [
            'attribution' => 'Tiles by <a target="attr" href="http://stamen.com">Stamen Design</a> under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. ',
          ]
        ]
      ]
    ];
// Get the JSON providing data.
//$request->addHeader('If-Modified-Since', gmdate(DATE_RFC1123, $last_fetched)); // TO RELAX ENDPOINT WORK
$response = $this->httpClient->request('GET', $this->entitiesUrl);
$entities_locations= json_decode($response->getBody()->getContents(), 'TRUE');
//FAKE TODO!!!!
/*$entities_locations= '[
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9998241,
                    39.7471494
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 51
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9983545,
                    39.7502833
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 52
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9963919,
                    39.7444271
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 54
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9960754,
                    39.7498956
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 55
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9933717,
                    39.7477264
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 57
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9913392,
                    39.7432392
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 58
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -104.9788452,
                    39.6933755
                ]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            "id": 74
        }
]';

$entities_locations = json_decode($entities_locations, 'TRUE');*/
    $features = [/*[
      'type' => 'point',
      'lat' =>  41.5667,
      'lon' => 2.0167,
      'popup' => 'me_popup',
      'leaflet_id' => 'some unique ID'
    ],*/
		[
			'type' => 'json',
			'json' => $entities_locations,
      'properties' => [
        'style' => [
          "color" => "#ff7800",
          "weight" => 5,
          "opacity" => 0.65
        ],
				'leaflet_id' => '555533'
			],
		]
  ];
    $height = "540px";
    $ll_map = $this->leaflet->leafletRenderMap($maps['mapnik'], $features, $height);
    $rendered_ll_map = render($ll_map);


    global $base_url;
    $form['show'] = [
      '#type' => 'submit',
      '#title' => $this->t('Show'),
      '#description' => $this->t('Shows the data in the vizualisation'),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['map'] = [
			'#type' => 'html_tag',
			'#tag' => 'div',
      '#value' => $rendered_ll_map,
    ];
    $form['#attached']['library'][] = 'd3_show/map_show-form';
    $form['#attached']['library'][] = 'd3_show/d3.d3js';
    $form['#attached']['drupalSettings']['baseUrl'] = $base_url;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }

  }

}
