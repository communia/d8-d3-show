<?php

namespace Drupal\d3_show\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class D3ShowForm.
 */
class D3ShowForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd3_show_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $form['show'] = [
      '#type' => 'submit',
      '#title' => $this->t('Show'),
      '#description' => $this->t('Shows the data in the vizualisation'),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#attached']['library'][] = 'd3_show/d3_show-form';
    $form['#attached']['library'][] = 'd3_show/d3.d3js';
    $form['#attached']['drupalSettings']['baseUrl'] = $base_url;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }

  }

}
