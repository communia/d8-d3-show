<?php

namespace Drupal\d3_show\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Controller for d3_show routes.
 */
class D3ShowController extends ControllerBase {

	/**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new D3ShowController
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The enitty querier
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(QueryFactory $entity_query, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityQuery = $entity_query;
		$this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns JSON data of form inputs.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the sorted numbers.
   */
  public function d3_show_json($data) {
    if (empty($data)) {
      $entities = $this->getEntities();
    }
    else if($data == "topojson"){
      $entities = $this->getEntitiesTopoJSON();
    }
    else if($data == "cities"){
      $entities = $this->getEntitiesCountByCity();
    }
    else {
      /* Can get form params as:
      $data_parts = explode('&', $data);
      foreach($data_parts as $part) {
        $fields[] = explode('=', $part);
      }
      // Loop through each field and grab values.
      foreach($fields as $field) {
        if (!empty($field[1])) {
          switch ($field[0]) {
            case 'numbers_total':
              $total = $field[1];;
              break;
            case integer_min:
              $range1 = $field[1];
              break;
            case integer_max:
              $range2 = $field[1];
              break;
          }
        }
      }
      // Sort the numbers.
      $numbers = $this->numbers_generate($total, $range1, $range2, FALSE);
      // FAKE THE RESULT!!!
      $numbers = $this->numbers_generate(7, 1, 99, FALSE); 
      */
			//$entities = $this->getEntities(); // We can get all entities
      $entities = $this->getEntitiesCountByCity(); // We can get aggrgated query entities
      //Or we can get params to alter the entity query.// TODO
			// TODO $entities = $this->getEntitiesFilter(xxx);// We can use $data incoming to alter the query
    }
    //FAKE the json:
    /*$bubbles = '{"name":"bubble", "children": [
{"name":"Atlas","description":"Atlas of Global Agriculture",
"children":[
    {"name": "Geography","address":"http://gli.environment.umn.edu", "note":"Global crop geography, including precipitation, temperature, crop area, etc."},
    {"name": "Crop Land","address":"http://d3js.org"},
    {"name": "Crop Yields","address":"http://environment.umn.edu", "note":"Maize, wheat, rice, and soybean yields in 2000"}
]},
{"name": "AgLab", "description": "Virtual Lab of Global Agriculture",
"children":[
    {"name":"Excess Nutrient","address":"http://d3js.org","note":"Prototype Infographics on Excess Fertilizer Nutrients"},
    {"name":"Yield Gap","address":"http://d3js.org", "note":"The gap between attainable yields and actual yields, with modeled yields assuming the percentage of gap closed."},
    {"name":"Fertilizer","address":"http://sunsp.net"}
]},
{"name":"Nutshell", "description":"Profiles of Country",
"children":[
    {"name":"Efficiency","address":"http://d3js.org"},
    {"name":"Excess Nutrient","address":"http://d3js.org"},
    {"name":"Economy","address":"http://d3js.org"},
    {"name":"Agriculture","address":"http://uis.edu/ens"}
]},
{"name":"Data", "description":"Crop Data in 5 minutes grid",
"children":[
    {"name":"Geography","address":"http://www.earthstat.org/"},
    {"name":"Crop Land","address":"http://www.earthstat.org/"},
    {"name":"Crop Yields","address":"http://www.earthstat.org/"}
]}
]}';*/
    
    //return new Response($bubbles);
    // Return a response as JSON.
    return new JsonResponse($entities);    
  }

  /**
   * Generates random numbers between delimiters.
   *
   * @param int $total
   *   The total number of bars.
   * @param int $range1
   *   The starting number.
   * @param int $range2
   *   The ending number.
   * @return array
   */
  private function numbers_generate($total = 10, $range1 = 1, $range2 = 100, $sort = FALSE) {
    $numbers = range($range1, $range2);
    shuffle($numbers);
    $numbers = array_slice($numbers, 0, $total);
    if ($sort) {
      rsort($numbers);
    }
    return $numbers;
  }

  /**
   * Gets the entities to build the array.
   *
   * @return array
   *   The structured array of entities with relevant fields.
   */
  private function getEntities(){
    $query = $this->entityQuery->get('group');
    $query->condition('type', "Entitat");
    $query->condition('field', $id);
    $entity_ids = $query->execute();
    return $entity_ids;
  }

  /**
   * Gets the entities grouped by cities to build the array.
   *
   * @return array
   *   The structured array of entities with relevant fields.
   */
  private function getEntitiesCountByCity(){
    // By now put in array, if more data, put in database,
    // such as:
    // https://github.com/antonrodin/municipios/blob/master/municipios.sql
    $cities = [
      'Terrassa' => ['geometry' => ['coordinates' => [41.560953, 2.0104398]]],
      'Castellar del Vallès' =>  ['geometry' => ['coordinates' => [41.6161765, 2.0856496]]],
      'Cerdanyola del Vallès' => ['geometry' =>  ['coordinates' => [41.4913697, 2.1407688]]],
      'La Llagosta' => ['geometry' => ['coordinates' => [41.5132133, 2.1932462]]],
      'Mollet del Vallès' => ['geometry' =>  ['coordinates' => [41.5405218, 2.2135289]]],
      'Montmeló' => ['geometry' =>  ['coordinates' => [41.5514602, 2.2484649]]],
      'Montornès del Vallès' => ['geometry' =>  ['coordinates' => [41.5425435, 2.2637794]]],
      'Sabadell' => ['geometry' =>  ['coordinates' => [41.5486198, 2.1074206]]],
      'Ullastrell' => ['geometry' =>  ['coordinates' => [41.5270691, 1.9569313]]]
    ];
    $query = $this->entityQuery->getAggregate('group');
    $query->condition('type', "Entitat");
    $query->groupBy('field_adreca.locality');
    $query->aggregate('id', 'COUNT');
    $entity_ids = $query->execute();
    $cities_entities = [];
    foreach ( $entity_ids as $id => $city){
      if ($cities[$city['field_adreca_locality']]){
        $cities[$city['field_adreca_locality']]['properties'] = [];
        $cities[$city['field_adreca_locality']]['properties']['entities_q'] = $city['id_count'];
        $cities[$city['field_adreca_locality']]['properties']['city'] = $city['field_adreca_locality'];
        $cities_entities[] = $cities[$city['field_adreca_locality']];
      }
    }
    return $cities_entities;
  }

  /**
   * Gets the entities with some fields to provide geojson array.
   *
   * @return array
   *   The structured array of entities with relevant fields.
   */
  private function getEntitiesTopoJSON(){
    $query = $this->entityQuery->get('group');
    $query->condition('type', "Entitat");
    $entity_ids = $query->execute();

    $groups = $this->entityTypeManager
      ->getStorage('group')
      ->loadMultiple($entity_ids);
    $topojson_groups = [];
    foreach ($groups as $group){
      if (!$group->field_geodata->lat && !$group->field_geodata->lon){
        continue;
      }
      if ($group->access('view')){
        $topojson_groups[] = [
          "geometry" => [
            "type" => "Point",
            "coordinates" => [ $group->field_geodata->lon, $group->field_geodata->lat],
          ],
          "type" => "Feature",
          "properties" => [
            "popup" => $group->label(),
          ],
          "id" => $group->id()
        ];
      }
    }
    return $topojson_groups;
  }
}
